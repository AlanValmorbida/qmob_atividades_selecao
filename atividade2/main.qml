import QtQuick 2.0
import QtQuick.Controls 1.6
import QtQuick.Window 2.0

Window {
    visible: true
    width: 1000
    height: 600
    title: qsTr("Atividade2")

    property var columns: [];
    property var rows: [];

    TableView {
        id: tableView
        anchors.fill: parent

        model: rows
        horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOn

        headerDelegate: Rectangle {
            height: 20
            color: "gray"
            border.color: "#BBBBBB"
            border.width: 1

            Text {
                text: styleData.value
                width: parent.width
                height: parent.height
                font.pointSize: 18
                minimumPointSize: 3
                fontSizeMode: Text.Fit
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
        }

        rowDelegate: Rectangle {
            color: styleData.selected ? "#0077CC" : styleData.row & 1 ? "white" : "#f5f5f5"
            height: 20
        }

        itemDelegate:
            Rectangle {
            color: styleData.selected ? "#0077CC" : styleData.row & 1 ? "white" : "#f5f5f5"
            border.color: "#BBBBBB"
            border.width: 1
            Text {
                text: styleData.value
            }
        }
    }

    Component {
        id: columnComponent
        TableViewColumn { width: tableView.width / columns.length }
    }

    function toUpperCase(c) { return c.toUpperCase(); }
    function capitalize(str) { return str.replace(/^./, toUpperCase); }

    function addColumn(name) {
        tableView.addColumn(columnComponent.createObject(tableView, { role: name, title: capitalize(name) } ) );
    }

    function setData(player_array) {
        columns = [];

        for (var i = 0; i < player_array.length; i++) {
            for (var proper in player_array[i]) {
                if (!columns.includes(proper) && typeof(player_array[i][proper]) !== "undefined") {
                    columns.push(proper);
                }
            }
        }

        columns.forEach(addColumn)

        rows = [];

        for (i = 0; i < player_array.length; i++) {
            var row_data = {}

            for (var j = 0; j < columns.length; j++) {
                var value = player_array[i].hasOwnProperty(columns[j]) ? String(player_array[i][columns[j]]) : String("");
                row_data[columns[j]] = value;
            }

            rows.push(row_data);

//            console.log("Array item:", player_array[i].ammunition)
//            console.log("Array item:", player_array[i])
//            listProperty(player_array[i]);
//            cities.push({name: "Melbourne", country: "Australia", subcountry: "Victoria", latitude: j, longitude: 144.7729583})
        }

        tableView.model = rows

//        console.log(rows)
    }
}

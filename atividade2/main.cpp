#include <QGuiApplication>
#include <QList>
#include <QMetaType>
#include <QQmlApplicationEngine>

#include "types/player.h"
#include "types/vehicle.h"

int main(int argc, char *argv[]) {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qRegisterMetaType<Player>();
    qRegisterMetaType<Vehicle>();

    QVariantList player_list;

    for (int i = 0; i < 5; i++) {
        Player player;
        player.m_speed = i * 100.1;
        player.m_ammunition = 10 - i;
        player.m_active = i % 2;
        player_list << QVariant::fromValue(player);
    }

    for (int i = 0; i < 2; i++) {
        Vehicle vehicle;
        vehicle.m_price = 1000 * i;
        vehicle.m_vehicleType = static_cast<Vehicle::VehicleType>(i);
        vehicle.m_position.m_x = (i + 1) * 10;
        vehicle.m_position.m_y = i * 2;
        player_list << QVariant::fromValue(vehicle);
    }

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(
            &engine, &QQmlApplicationEngine::objectCreated, &app,
            [url](QObject *obj, const QUrl &objUrl) {
                if (!obj && url == objUrl)
                    QCoreApplication::exit(-1);
            },
            Qt::QueuedConnection);
    engine.load(url);

    QMetaObject::invokeMethod(engine.rootObjects().first(), "setData",
            Q_ARG(QVariant, QVariant::fromValue(player_list)));

    return app.exec();
}

#ifndef PLAYER_H
#define PLAYER_H

#include <QMetaType>

#include "types/p2d.h"

namespace player_editor {
Q_NAMESPACE

enum class PlayerType { NO_TYPE = 0, TANK, CHARACTER, SHIP };

Q_ENUM_NS(PlayerType)
}  // namespace player_editor

struct Player {
    Q_GADGET
    Q_PROPERTY(player_editor::PlayerType playerType MEMBER m_playerType)
    Q_PROPERTY(float speed MEMBER m_speed)
    Q_PROPERTY(quint16 ammunition MEMBER m_ammunition)
    Q_PROPERTY(bool active MEMBER m_active)
    Q_PROPERTY(quint8 numberOfCoordinates MEMBER m_numberOfCoordinates)
    Q_PROPERTY(P2D *coordinates MEMBER m_coordinates)

 public:
    Player()
            : m_playerType(player_editor::PlayerType::NO_TYPE),
              m_speed(0.0),
              m_ammunition(0),
              m_active(false),
              m_numberOfCoordinates(0),
              m_coordinates(nullptr) {
    }

    bool operator==(Player const &other) {
        return (m_playerType == other.m_playerType && m_speed == other.m_speed &&
                m_ammunition == other.m_ammunition && m_active == other.m_active &&
                m_numberOfCoordinates == other.m_numberOfCoordinates &&
                m_coordinates == other.m_coordinates);
    }

    bool operator!=(Player const &other) {
        return !operator==(other);
    }

    player_editor::PlayerType m_playerType;
    float m_speed;
    quint16 m_ammunition;
    bool m_active;
    quint8 m_numberOfCoordinates;
    P2D *m_coordinates;
};

Q_DECLARE_METATYPE(Player)

#endif  // PLAYER_H

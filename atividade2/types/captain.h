#ifndef CAPTAIN_H
#define CAPTAIN_H

#include <QMetaType>
#include <QString>

#include "types/player.h"
#include "types/vehicle.h"

struct Captain {
    Q_GADGET
    Q_PROPERTY(QString name MEMBER m_name)
    Q_PROPERTY(Player player MEMBER m_player)
    Q_PROPERTY(Vehicle vehicle MEMBER m_vehicle)
    Q_PROPERTY(quint8 armySize MEMBER m_armySize)
    Q_PROPERTY(Player *players MEMBER m_players)

 public:
    Captain() : m_name("Captain"), m_armySize(0), m_players(nullptr) {
    }

    QString m_name;
    Player m_player;
    Vehicle m_vehicle;
    quint8 m_armySize;
    Player *m_players;
};

Q_DECLARE_METATYPE(Captain)

#endif  // CAPTAIN_H

#ifndef VEHICLE_H
#define VEHICLE_H

#include <QMetaType>

#include "types/p2d.h"
#include "types/p3d.h"

struct Vehicle {
    Q_GADGET
    Q_PROPERTY(VehicleType vehicleType MEMBER m_vehicleType)
    Q_PROPERTY(quint32 price MEMBER m_price)
    Q_PROPERTY(P2D position MEMBER m_position)
    Q_PROPERTY(quint8 numberOfCoordinates MEMBER m_numberOfCoordinates)
    Q_PROPERTY(P3D *coordinates MEMBER m_coordinates)

 public:
    Vehicle()
            : m_vehicleType(VehicleType::NO_TYPE),
              m_price(0),
              m_position(0, 0),
              m_numberOfCoordinates(0),
              m_coordinates(nullptr) {
    }

    bool operator==(Vehicle const &other) {
        return (m_vehicleType == other.m_vehicleType && m_price == other.m_price &&
                m_position == other.m_position &&
                m_numberOfCoordinates == other.m_numberOfCoordinates &&
                m_coordinates == other.m_coordinates);
    }

    bool operator!=(Vehicle const &other) {
        return !operator==(other);
    }

    enum class VehicleType { NO_TYPE = 2, CAR, TRAIN, WATERCRAFT = 7, AIRCRAFT, SPACECRAFT };
    Q_ENUM(VehicleType)

    VehicleType m_vehicleType;
    quint32 m_price;
    P2D m_position;
    quint8 m_numberOfCoordinates;
    P3D *m_coordinates;
};

Q_DECLARE_METATYPE(Vehicle)

#endif  // VEHICLE_H

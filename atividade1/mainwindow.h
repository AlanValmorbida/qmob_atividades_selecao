#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>

#include "types/captain.h"
#include "types/player.h"
#include "types/vehicle.h"

class MainWindow : public QMainWindow {
    Q_OBJECT

 public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

 private:
    Player player_;
    Vehicle vehicle_;
    Captain captain_;
};
#endif  // MAINWINDOW_H

#include "playereditor.h"

#include <stdlib.h>

#include <QCheckBox>
#include <QComboBox>
#include <QDebug>
#include <QDoubleSpinBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QMetaProperty>
#include <QSpinBox>
#include <limits>

PlayerEditor::PlayerEditor(QWidget *parent, const QString &type_name, void *q_gadget)
        : QDialog(parent), type_name_(type_name), q_gadget_(q_gadget) {
    resize(640, 480);

    main_layout_ = new QVBoxLayout(this);
    scroll_area_ = new QScrollArea(this);

    QWidget *scroll_widget = new QWidget(this);
    scroll_widget->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    scroll_widget->setContentsMargins(0, 0, 0, 0);
    scroll_widget->setFixedWidth(scroll_area_->width());
    scroll_area_->setWidget(scroll_widget);
    scroll_area_->setWidgetResizable(true);

    main_layout_->addWidget(scroll_area_);

    save_button_ = new QPushButton(tr("Save"), this);
    connect(save_button_, &QPushButton::clicked, this, &PlayerEditor::Save);

    cancel_button_ = new QPushButton(tr("Cancel"), this);
    connect(cancel_button_, &QPushButton::clicked, this, &QDialog::reject);

    QHBoxLayout *buttons_layout = new QHBoxLayout();
    buttons_layout->addWidget(save_button_, 0, Qt::AlignCenter);
    buttons_layout->addWidget(cancel_button_, 0, Qt::AlignCenter);
    main_layout_->addLayout(buttons_layout);

    int type_id = QMetaType::type(type_name.toLocal8Bit().constData());
    if (type_id != QMetaType::UnknownType) {
        const QMetaObject *meta_object = QMetaType::metaObjectForType(type_id);

        editor_layout_ = CreateEditorLayout(meta_object, q_gadget);
        editor_layout_->addWidget(new QWidget(this));
        editor_layout_->setRowStretch(editor_layout_->rowCount() - 1, 1);

        scroll_widget->setLayout(editor_layout_);
    } else {
        qDebug() << "ERROR: UnknownType";
    }
}

PlayerEditor::~PlayerEditor() {
}

int PlayerEditor::exec() {
    if (QMetaType::type(type_name_.toLocal8Bit().constData()) == QMetaType::UnknownType) {
        QMessageBox msg_box(this);
        msg_box.setText("Error: UnknownType");
        msg_box.setDefaultButton(QMessageBox::Ok);

        msg_box.exec();
        return QDialog::Rejected;
    }
    return QDialog::exec();
}

QGridLayout *PlayerEditor::CreateEditorLayout(const QMetaObject *meta_object, void *q_gadget) {
    QGridLayout *layout = new QGridLayout();
    layout->setSizeConstraint(QLayout::SetMaximumSize);
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 1);

    QLabel *label = nullptr;
    QWidget *widget = nullptr;

    int property_count = meta_object->propertyCount();
    int line_count = 0;
    for (int i = 0; i < property_count; ++i) {
        QMetaProperty meta_property = meta_object->property(i);

        if (meta_property.isEnumType()) {
            QComboBox *combo_box = new QComboBox(this);
            widget = combo_box;

            QMetaEnum meta_enum = meta_property.enumerator();

            int key_count = meta_enum.keyCount();
            for (int j = 0; j < key_count; j++) {
                combo_box->addItem(meta_enum.key(j));
            }

            combo_box->setCurrentText(
                    meta_enum.valueToKey(meta_property.readOnGadget(q_gadget).toInt()));
        } else {
            switch (static_cast<QMetaType::Type>(meta_property.type())) {
                case QMetaType::Float:
                case QMetaType::Double: {
                    QDoubleSpinBox *spin_box = new QDoubleSpinBox(this);
                    spin_box->setMinimum(std::numeric_limits<float>::lowest());
                    spin_box->setMaximum(std::numeric_limits<float>::max());
                    spin_box->setValue(meta_property.readOnGadget(q_gadget).toDouble());
                    widget = spin_box;
                } break;
                case QMetaType::UInt: {
                    QSpinBox *spin_box = new QSpinBox(this);
                    spin_box->setMinimum(0);
                    spin_box->setMaximum(static_cast<int>(std::numeric_limits<qint32>::max()));
                    spin_box->setValue(meta_property.readOnGadget(q_gadget).toUInt());
                    widget = spin_box;
                } break;
                case QMetaType::Int: {
                    QSpinBox *spin_box = new QSpinBox(this);
                    spin_box->setMinimum(std::numeric_limits<qint32>::min());
                    spin_box->setMaximum(std::numeric_limits<qint32>::max());
                    spin_box->setValue(meta_property.readOnGadget(q_gadget).toInt());
                    widget = spin_box;
                } break;
                case QMetaType::UShort: {
                    QSpinBox *spin_box = new QSpinBox(this);
                    spin_box->setMinimum(std::numeric_limits<quint16>::min());
                    spin_box->setMaximum(std::numeric_limits<quint16>::max());
                    spin_box->setValue(meta_property.readOnGadget(q_gadget).toUInt());
                    widget = spin_box;
                } break;
                case QMetaType::Short: {
                    QSpinBox *spin_box = new QSpinBox(this);
                    spin_box->setMinimum(std::numeric_limits<qint16>::min());
                    spin_box->setMaximum(std::numeric_limits<qint16>::max());
                    spin_box->setValue(meta_property.readOnGadget(q_gadget).toInt());
                    widget = spin_box;
                } break;
                case QMetaType::UChar: {
                    QSpinBox *spin_box = new QSpinBox(this);
                    spin_box->setMinimum(std::numeric_limits<quint8>::min());
                    spin_box->setMaximum(std::numeric_limits<quint8>::max());
                    spin_box->setValue(meta_property.readOnGadget(q_gadget).toUInt());
                    widget = spin_box;
                } break;
                case QMetaType::Char: {
                    QSpinBox *spin_box = new QSpinBox(this);
                    spin_box->setMinimum(std::numeric_limits<qint8>::min());
                    spin_box->setMaximum(std::numeric_limits<qint8>::max());
                    spin_box->setValue(meta_property.readOnGadget(q_gadget).toInt());
                    widget = spin_box;
                } break;
                case QMetaType::Bool: {
                    QCheckBox *check_box = new QCheckBox(this);
                    check_box->setChecked(meta_property.readOnGadget(q_gadget).toBool());
                    widget = check_box;
                } break;
                case QMetaType::QString: {
                    QLineEdit *line_edit = new QLineEdit(this);
                    line_edit->setText(meta_property.readOnGadget(q_gadget).toString());
                    widget = line_edit;
                } break;
                default: {
                    if (QString(meta_property.typeName()).back() == '*') {
                        QVariant ptr_variant = meta_property.readOnGadget(q_gadget);

                        if (!ptr_variant.canConvert(QMetaType::VoidStar)) {
                            qDebug() << "Error converting" << meta_property.typeName()
                                     << "to void*, please use QMetaType::registerConverter<"
                                     << meta_property.typeName()
                                     << ", void *>() to register the converter";
                            break;
                        }

                        if (i < 1) {
                            break;
                        }

                        QVariant variant = meta_object->property(i - 1).readOnGadget(q_gadget);

                        if (!variant.canConvert(QMetaType::UInt)) {
                            break;
                        }

                        int base_type_id = GetBaseType(meta_property.typeName());
                        if (base_type_id == QMetaType::UnknownType) {
                            qDebug() << "Error getting base type from:" << meta_property.typeName();
                            break;
                        }

                        QFrame *frame = new QFrame(this);
                        frame->setFrameStyle(QFrame::StyledPanel);

                        QGridLayout *editor_layout = new QGridLayout();
                        editor_layout->setColumnStretch(0, 0);
                        editor_layout->setColumnStretch(1, 1);
                        frame->setLayout(editor_layout);

                        const QMetaObject *type_meta_object =
                                QMetaType::metaObjectForType(base_type_id);
                        void *gadget = meta_property.readOnGadget(q_gadget).value<void *>();

                        int inst_count = variant.toUInt();
                        for (int j = 0; j < inst_count; ++j) {
                            editor_layout->addWidget(new QLabel(RemoveLastS(meta_property.name()) +
                                                                        QString::number(j + 1),
                                                                this),
                                                     j, 0, Qt::AlignRight);

                            void *current_gadget =
                                    ((uint8_t *)gadget) + j * QMetaType::sizeOf(base_type_id);

                            QFrame *frame = new QFrame(this);
                            frame->setLayout(CreateEditorLayout(type_meta_object, current_gadget));
                            frame->setFrameStyle(QFrame::StyledPanel);

                            editor_layout->addWidget(frame, j, 1);
                        }

                        connect(dynamic_cast<QSpinBox *>(
                                        layout->itemAtPosition(line_count - 1, 1)->widget()),
                                QOverload<int>::of(&QSpinBox::valueChanged), this,
                                [this, editor_layout, meta_property](int value) {
                                    UpdateNumberOfElements(editor_layout, meta_property, value);
                                });

                        widget = frame;
                    } else {
                        int type_id = QMetaType::type(meta_property.typeName());
                        const QMetaObject *type_meta_object = QMetaType::metaObjectForType(type_id);

                        QGridLayout *editor_layout = CreateEditorLayout(
                                type_meta_object, meta_property.readOnGadget(q_gadget).data());

                        QFrame *frame = new QFrame(this);
                        frame->setLayout(editor_layout);
                        frame->setFrameStyle(QFrame::StyledPanel);
                        widget = frame;
                    }
                } break;
            }
        }

        if (widget) {
            widget->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
            label = new QLabel(meta_property.name(), this);
            layout->addWidget(label, line_count, 0, Qt::AlignRight);
            layout->addWidget(widget, line_count, 1);
            line_count++;
        }

        widget = nullptr;
    }
    return layout;
}

void PlayerEditor::Save() {
    int type_id = QMetaType::type(type_name_.toLocal8Bit().constData());
    if (type_id != QMetaType::UnknownType) {
        const QMetaObject *meta_object = QMetaType::metaObjectForType(type_id);

        UpdateValues(meta_object, q_gadget_, editor_layout_);
    }

    accept();
}

void PlayerEditor::UpdateValues(const QMetaObject *meta_object, void *q_gadget,
                                const QGridLayout *editor_layout) {
    int property_count = meta_object->propertyCount();
    int line_count = 0;
    QVariant last_old_variant;
    QVariant last_variant;
    for (int i = 0; i < property_count; ++i) {
        QMetaProperty meta_property = meta_object->property(i);
        QVariant variant;
        QVariant old_variant = meta_property.readOnGadget(q_gadget);

        QLayoutItem *layout_item = editor_layout->itemAtPosition(line_count, 1);

        if (layout_item == nullptr) {
            return;
        }

        QWidget *widget = layout_item->widget();

        if (widget == nullptr) {
            return;
        }

        if (meta_property.isEnumType()) {
            QComboBox *combo_box = dynamic_cast<QComboBox *>(widget);

            QMetaEnum meta_enum = meta_property.enumerator();
            variant = meta_enum.value(combo_box->currentIndex());
        } else {
            switch (static_cast<QMetaType::Type>(meta_property.type())) {
                case QMetaType::Float:
                case QMetaType::Double: {
                    QDoubleSpinBox *spin_box = dynamic_cast<QDoubleSpinBox *>(widget);
                    variant = spin_box->value();
                } break;
                case QMetaType::UInt:
                case QMetaType::Int:
                case QMetaType::UShort:
                case QMetaType::Short:
                case QMetaType::UChar:
                case QMetaType::Char: {
                    QSpinBox *spin_box = dynamic_cast<QSpinBox *>(widget);
                    variant = spin_box->value();
                } break;
                case QMetaType::Bool: {
                    QCheckBox *check_box = dynamic_cast<QCheckBox *>(widget);
                    variant = check_box->isChecked();
                } break;
                case QMetaType::QString: {
                    QLineEdit *line_edit = dynamic_cast<QLineEdit *>(widget);
                    variant = line_edit->text();
                } break;
                default: {
                    if (QString(meta_property.typeName()).back() == '*') {
                        if (i < 1) {
                            break;
                        }

                        if (!last_variant.canConvert(QMetaType::UInt)) {
                            break;
                        }

                        int base_type_id = GetBaseType(meta_property.typeName());
                        if (base_type_id == QMetaType::UnknownType) {
                            qDebug() << "Error getting base type from:" << meta_property.typeName();
                            break;
                        }

                        int inst_count = last_variant.toUInt();
                        int last_inst_count = last_old_variant.toUInt();
                        void *gadget = meta_property.readOnGadget(q_gadget).value<void *>();

                        if (last_inst_count == 0) {
                            gadget = nullptr;
                        }

                        int type_id = QMetaType::type(meta_property.typeName());
                        const QMetaObject *base_meta_object = QMetaType::metaObjectForType(base_type_id);

                        for (int j = (last_inst_count - 1); j >= inst_count; --j) {
                            void *current_gadget =
                                    ((uint8_t *)gadget) + j * QMetaType::sizeOf(base_type_id);
                            DeleteSubElements(base_meta_object, current_gadget);
                        }

                        gadget = realloc(gadget, inst_count * QMetaType::sizeOf(base_type_id));

                        QGridLayout *layout = dynamic_cast<QGridLayout *>(widget->layout());

                        for (int j = 0; j < inst_count; ++j) {
                            void *current_gadget =
                                    ((uint8_t *)gadget) + j * QMetaType::sizeOf(base_type_id);
                            QMetaType::construct(base_type_id, current_gadget, nullptr);
                            UpdateValues(base_meta_object, current_gadget,
                                         dynamic_cast<QGridLayout *>(
                                                 layout->itemAtPosition(j, 1)->widget()->layout()));
                        }

                        variant = QVariant(type_id, &gadget);

                        // forces write, because this variant is interpreted as invalid ¯\(°_°)/¯
                        meta_property.writeOnGadget(q_gadget, variant);
                        line_count++;
                        variant.clear();
                    } else {
                        int type_id = QMetaType::type(meta_property.typeName());
                        const QMetaObject *metaObject = QMetaType::metaObjectForType(type_id);
                        QVariant gadget_var = meta_property.readOnGadget(q_gadget);
                        void *gadget = gadget_var.data();

                        UpdateValues(metaObject, gadget,
                                     dynamic_cast<QGridLayout *>(widget->layout()));

                        variant = gadget_var;
                    }
                } break;
            }
        }

        if (variant.isValid()) {
            meta_property.writeOnGadget(q_gadget, variant);
            line_count++;
        }

        last_variant = variant;
        last_old_variant = old_variant;
    }
}

void PlayerEditor::UpdateNumberOfElements(QGridLayout *editor_layout,
                                          const QMetaProperty &meta_property,
                                          int number_of_elements) {
    int row_count = editor_layout->rowCount();

    for (int i = editor_layout->rowCount() - 1; i >= 0; --i) {
        QLayoutItem *layout_item = editor_layout->itemAtPosition(i, 0);
        if (layout_item == nullptr || layout_item->widget() == nullptr) {
            row_count--;
        } else {
            break;
        }
    }

    if (number_of_elements == row_count) {
        return;
    } else if (number_of_elements < row_count) {
        for (int i = (row_count - 1); i >= number_of_elements; --i) {
            QLayoutItem *item = editor_layout->itemAtPosition(i, 0);
            item->widget()->deleteLater();

            item = editor_layout->itemAtPosition(i, 1);
            item->widget()->deleteLater();
        }
    } else {
        int type_id = GetBaseType(meta_property.typeName());
        const QMetaObject *meta_object = QMetaType::metaObjectForType(type_id);
        void *gadget = QMetaType::create(type_id, nullptr);

        for (int i = row_count; i < number_of_elements; ++i) {
            editor_layout->addWidget(
                    new QLabel(RemoveLastS(meta_property.name()) + QString::number(i + 1), this), i,
                    0, Qt::AlignRight);

            QFrame *frame = new QFrame(this);
            frame->setLayout(CreateEditorLayout(meta_object, gadget));
            frame->setFrameStyle(QFrame::StyledPanel);

            editor_layout->addWidget(frame, i, 1);
        }

        QMetaType::destroy(type_id, gadget);
    }
}

void PlayerEditor::DeleteSubElements(const QMetaObject *meta_object, void *q_gadget) {
    int property_count = meta_object->propertyCount();
    for (int i = 0; i < property_count; ++i) {
        QMetaProperty meta_property = meta_object->property(i);

        if (QString(meta_property.typeName()).back() == '*') {
            if (i < 1) {
                continue;
            }

            QVariant inst_count_variant = meta_object->property(i - 1).readOnGadget(q_gadget);

            if (!inst_count_variant.canConvert(QMetaType::UInt)) {
                continue;
            }

            int inst_count = inst_count_variant.toUInt();

            if (inst_count == 0) {
                continue;
            }

            int base_type_id = GetBaseType(meta_property.typeName());
            if (base_type_id == QMetaType::UnknownType) {
                qDebug() << "Error getting base type from:" << meta_property.typeName();
                continue;
            }

            const QMetaObject *base_meta_object = QMetaType::metaObjectForType(base_type_id);
            void *gadget = meta_property.readOnGadget(q_gadget).value<void *>();

            for (int j = 0; j < inst_count; ++j) {
                void *current_gadget = ((uint8_t *)gadget) + j * QMetaType::sizeOf(base_type_id);

                DeleteSubElements(base_meta_object, current_gadget);
            }

            free(gadget);
        }
    }
}

int PlayerEditor::GetBaseType(QString type_name) {
    type_name.chop(1);

    return QMetaType::type(type_name.toLocal8Bit().constData());
}

QString PlayerEditor::RemoveLastS(QString text) {
    if (text.back() == 's') {
        text.chop(1);
    }
    return text;
}

#ifndef P3D_H
#define P3D_H

#include <QMetaType>

struct P3D {
    Q_GADGET
    Q_PROPERTY(float x MEMBER m_x)
    Q_PROPERTY(float y MEMBER m_y)
    Q_PROPERTY(float z MEMBER m_z)

 public:
    P3D() : m_x(0.0), m_y(0.0), m_z(0.0) {
    }

    P3D(float x, float y, float z) : m_x(x), m_y(y), m_z(z) {
    }

    bool operator==(P3D const &other) {
        return (m_x == other.m_x && m_y == other.m_y && m_z == other.m_z);
    }

    bool operator!=(P3D const &other) {
        return !operator==(other);
    }

    float m_x;
    float m_y;
    float m_z;
};

Q_DECLARE_METATYPE(P3D)

#endif  // P3D_H

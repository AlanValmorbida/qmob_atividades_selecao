#ifndef P2D_H
#define P2D_H

#include <QMetaType>

struct P2D {
    Q_GADGET
    Q_PROPERTY(float x MEMBER m_x)
    Q_PROPERTY(float y MEMBER m_y)

 public:
    P2D() : m_x(0.0), m_y(0.0) {
    }

    P2D(float x, float y) : m_x(x), m_y(y) {
    }

    bool operator==(P2D const &other) {
        return (m_x == other.m_x && m_y == other.m_y);
    }

    bool operator!=(P2D const &other) {
        return !operator==(other);
    }

    float m_x;
    float m_y;
};

Q_DECLARE_METATYPE(P2D)

#endif  // P2D_H

#include "mainwindow.h"

#include <QMetaType>
#include <QVBoxLayout>

#include "playereditor.h"
#include "types/p2d.h"
#include "types/p3d.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
    qRegisterMetaType<P2D>();
    qRegisterMetaType<P2D *>();
    qRegisterMetaType<P3D>();
    qRegisterMetaType<P3D *>();
    qRegisterMetaType<Player>();
    qRegisterMetaType<Player *>();
    qRegisterMetaType<Vehicle>();
    qRegisterMetaType<Vehicle *>();
    qRegisterMetaType<Captain>();

    QMetaType::registerConverter<P2D *, void *>();
    QMetaType::registerConverter<P3D *, void *>();
    QMetaType::registerConverter<Player *, void *>();

    QVBoxLayout *main_layout = new QVBoxLayout();
    QWidget *widget = new QWidget();
    widget->setLayout(main_layout);
    setCentralWidget(widget);

    QPushButton *button = new QPushButton(tr("Edit Player"), this);
    main_layout->addWidget(button, 0, Qt::AlignCenter);

    connect(button, &QPushButton::clicked, this, [this]() {
        PlayerEditor player_editor(this, player_.staticMetaObject.className(), &player_);
        player_editor.exec();
    });

    button = new QPushButton(tr("Edit Vehicle"), this);
    main_layout->addWidget(button, 0, Qt::AlignCenter);

    connect(button, &QPushButton::clicked, this, [this]() {
        PlayerEditor player_editor(this, vehicle_.staticMetaObject.className(), &vehicle_);
        player_editor.exec();
    });

    button = new QPushButton(tr("Edit Captain"), this);
    main_layout->addWidget(button, 0, Qt::AlignCenter);

    connect(button, &QPushButton::clicked, this, [this]() {
        PlayerEditor player_editor(this, captain_.staticMetaObject.className(), &captain_);
        player_editor.exec();
    });

    button = new QPushButton(tr("Edit Invalid"), this);
    main_layout->addWidget(button, 0, Qt::AlignCenter);

    connect(button, &QPushButton::clicked, this, [this]() {
        PlayerEditor player_editor(this, "", nullptr);
        player_editor.exec();
    });
}

MainWindow::~MainWindow() {
}

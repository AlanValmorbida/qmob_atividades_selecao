#ifndef PLAYEREDITOR_H
#define PLAYEREDITOR_H

#include <QDialog>
#include <QGridLayout>
#include <QPushButton>
#include <QScrollArea>
#include <QString>
#include <QVBoxLayout>

class PlayerEditor : public QDialog {
    Q_OBJECT
 public:
    explicit PlayerEditor(QWidget *parent, const QString &type_name, void *q_gadget);

    ~PlayerEditor();

    int exec() override;

 private:
    QGridLayout *CreateEditorLayout(const QMetaObject *meta_object, void *q_gadget);

    void Save();
    void UpdateValues(const QMetaObject *meta_object, void *q_gadget,
                      const QGridLayout *editor_layout);

    void UpdateNumberOfElements(QGridLayout *editor_layout, const QMetaProperty &meta_property,
                                int number_of_elements);

    void DeleteSubElements(const QMetaObject *meta_object, void *q_gadget);

    int GetBaseType(QString type_name);

    QString RemoveLastS(QString text);

    QString type_name_;
    void *q_gadget_;

    QVBoxLayout *main_layout_;
    QScrollArea *scroll_area_;
    QGridLayout *editor_layout_;
    QPushButton *save_button_;
    QPushButton *cancel_button_;
};

#endif  // PLAYEREDITOR_H
